Rails.application.routes.draw do

  devise_for :users, path: '', path_names: { sign_in: 'login', sign_out: 'logout', password: 'secret', confirmation: 'verification', unlock: 'unblock', registration: 'register', sign_up: 'cmon_let_me_in' } 

  resources :users
  root to: "home#index"

  resources :enrollments
  resources :presences
  resources :major_disciplines
  resources :professors
  resources :classrooms
  resources :periods
  resources :lectures
  resources :students
  resources :disciplines
  resources :majors
  resources :departments
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
