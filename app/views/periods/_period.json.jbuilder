json.extract! period, :id, :happens, :created_at, :updated_at
json.url period_url(period, format: :json)
