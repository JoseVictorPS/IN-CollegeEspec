json.extract! student, :id, :major, :name, :created_at, :updated_at
json.url student_url(student, format: :json)
