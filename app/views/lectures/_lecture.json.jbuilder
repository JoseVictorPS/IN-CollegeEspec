json.extract! lecture, :id, :classroom, :name, :created_at, :updated_at
json.url lecture_url(lecture, format: :json)
