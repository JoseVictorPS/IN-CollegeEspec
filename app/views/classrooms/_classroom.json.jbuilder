json.extract! classroom, :id, :period, :discipline, :professor, :created_at, :updated_at
json.url classroom_url(classroom, format: :json)
