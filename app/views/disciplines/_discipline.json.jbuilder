json.extract! discipline, :id, :department, :name, :created_at, :updated_at
json.url discipline_url(discipline, format: :json)
