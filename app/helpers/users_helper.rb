module UsersHelper

    def user_kind_options
        [['Administrador', :admin],['Padrão', :standard],["Professor", :teacher],["Coordenador", :coord]]
    end

    def resource_name
        :user
    end

    def resource
        @resource ||= User.new
    end

    def devise_mapping
        @devise_mapping ||= Devise.mappings[:user]
    end

end
