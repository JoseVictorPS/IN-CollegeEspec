class Classroom < ApplicationRecord
    belongs_to :professor
    belongs_to :period
    belongs_to :discipline
    has_many :lectures

    has_many :enrollments
    has_many :students, through: :enrollments

end
