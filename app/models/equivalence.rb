class Equivalence < ApplicationRecord
    belongs_to :discipline
    belongs_to :eq_discipline, class_name: 'Discipline'
end
