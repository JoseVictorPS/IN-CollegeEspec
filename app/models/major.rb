class Major < ApplicationRecord
    belongs_to :department
    has_many :students

    has_many :major_disciplines
    has_many :disciplines, through: :major_disciplines
end
