class Professor < ApplicationRecord
    has_many :classrooms
    belongs_to :department
    belongs_to :user
end