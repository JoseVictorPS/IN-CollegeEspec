class MajorDiscipline < ApplicationRecord
  belongs_to :major
  belongs_to :discipline
end
