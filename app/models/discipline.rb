class Discipline < ApplicationRecord
    belongs_to :department
    belongs_to :classroom, optional: true
    has_and_belongs_to_many :equivalences,
        class_name: "Discipline",
        join_table: :equivalences,
        foreign_key: :disc_id,
        association_foreign_key: :eq_disc_id

    has_many :major_disciplines
    has_many :majors, through: :major_disciplines

    has_many :equivalences
    has_many :eq_disciplines, through: :equivalences

    has_many :equivalents, foreign_key: :eq_discipline_id, class_name: 'Equivalence'
    has_many :equivalent_disciplines, through: :equivalents, source: :discipline

end
