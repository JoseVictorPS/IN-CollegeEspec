class Student < ApplicationRecord
    belongs_to :major

    has_many :enrollments
    has_many :classrooms, through: :enrollments

    has_many :presences
    has_many :lectures, through: :presences

end
