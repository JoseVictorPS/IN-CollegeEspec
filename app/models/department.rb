class Department < ApplicationRecord
    has_many :professors
    has_many :discipline
    has_many :major
end
