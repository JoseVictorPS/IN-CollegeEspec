class CreateClassrooms < ActiveRecord::Migration[5.2]
  def change
    create_table :classrooms do |t|
      t.references :period, foreign_key: true
      t.references :discipline, foreign_key: true
      t.references :professor, foreign_key: true

      t.timestamps
    end
  end
end
