class CreateEquivalenceTable < ActiveRecord::Migration[5.2]
  def change
    create_table :equivalences do |t|
      t.integer :discipline_id
      t.integer :eq_discipline_id
    end

    add_index(:equivalences, [:discipline_id, :eq_discipline_id], :unique => true)
    add_index(:equivalences, [:eq_discipline_id, :discipline_id], :unique => true)
  end
end
