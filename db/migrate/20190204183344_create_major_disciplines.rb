class CreateMajorDisciplines < ActiveRecord::Migration[5.2]
  def change
    create_table :major_disciplines do |t|
      t.references :major, foreign_key: true
      t.references :discipline, foreign_key: true

      t.timestamps
    end
  end
end
