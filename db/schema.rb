# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_02_08_175148) do

  create_table "classrooms", force: :cascade do |t|
    t.integer "period_id"
    t.integer "discipline_id"
    t.integer "professor_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.index ["discipline_id"], name: "index_classrooms_on_discipline_id"
    t.index ["period_id"], name: "index_classrooms_on_period_id"
    t.index ["professor_id"], name: "index_classrooms_on_professor_id"
  end

  create_table "departments", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "disciplines", force: :cascade do |t|
    t.integer "department_id"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["department_id"], name: "index_disciplines_on_department_id"
  end

  create_table "enrollments", force: :cascade do |t|
    t.integer "classroom_id"
    t.integer "student_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["classroom_id"], name: "index_enrollments_on_classroom_id"
    t.index ["student_id"], name: "index_enrollments_on_student_id"
  end

  create_table "equivalences", force: :cascade do |t|
    t.integer "discipline_id"
    t.integer "eq_discipline_id"
    t.index ["discipline_id", "eq_discipline_id"], name: "index_equivalences_on_discipline_id_and_eq_discipline_id", unique: true
    t.index ["eq_discipline_id", "discipline_id"], name: "index_equivalences_on_eq_discipline_id_and_discipline_id", unique: true
  end

  create_table "lectures", force: :cascade do |t|
    t.integer "classroom_id"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["classroom_id"], name: "index_lectures_on_classroom_id"
  end

  create_table "major_disciplines", force: :cascade do |t|
    t.integer "major_id"
    t.integer "discipline_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["discipline_id"], name: "index_major_disciplines_on_discipline_id"
    t.index ["major_id"], name: "index_major_disciplines_on_major_id"
  end

  create_table "majors", force: :cascade do |t|
    t.integer "department_id"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["department_id"], name: "index_majors_on_department_id"
  end

  create_table "periods", force: :cascade do |t|
    t.string "happens"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "presences", force: :cascade do |t|
    t.integer "student_id"
    t.integer "lecture_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["lecture_id"], name: "index_presences_on_lecture_id"
    t.index ["student_id"], name: "index_presences_on_student_id"
  end

  create_table "professors", force: :cascade do |t|
    t.integer "department_id"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_id"
    t.index ["department_id"], name: "index_professors_on_department_id"
    t.index ["user_id"], name: "index_professors_on_user_id"
  end

  create_table "students", force: :cascade do |t|
    t.integer "major_id"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["major_id"], name: "index_students_on_major_id"
  end

  create_table "users", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "photo"
    t.integer "kind"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
